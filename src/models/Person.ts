export class Person {
    dp: string;
    id: number;
    firstName: string;
    lastName: string;
    username: string;
    token: string;
    email: string;
    password: string;
    type: string;

    constructor() {
        this.firstName = this.lastName = this.email = this.password = '';
        this.type = 'user';
    }

}


