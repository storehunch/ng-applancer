export class Reply {
    id: number;
    replyTo: number;
    content: string;

    constructor(private userId: number, private forumThreadId: number, private title: string) {
        this.content = '';
    }

}


