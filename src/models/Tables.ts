export enum Tables {
    Companies, ForumThreads, ForumTopics, People, Replies
}

export enum Include {
    none,
    owner, company, forumThreads, replies
}
