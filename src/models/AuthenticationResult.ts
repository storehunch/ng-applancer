export interface AuthenticationResult {
    id: string;
    ttl: number;
    created: Date;
    personId: number;
}
