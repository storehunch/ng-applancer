export class ForumThread {
    id: number;
    forumTopicId: number;
    title: string;
    content: string;

    constructor(private userId: number) {
        this.title = this.content = '';
        this.forumTopicId = 0;
    }

}


