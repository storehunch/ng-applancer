import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SigninComponent } from './signin/signin.component';
import { SharedHttpService } from './services/shared-http.service';
import { SignupComponent } from './signup/signup.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { ForumComponent } from './forum/forum.component';
import { ForumThreadsComponent } from './forum-threads/forum-threads.component';
import { ForumThreadInnerComponent } from './forum-thread-inner/forum-thread-inner.component';
import { NewThreadComponent } from './new-thread/new-thread.component';
import { BaseComponent } from './BaseComponent/BaseComponent';
import { ReplyComponent } from './reply/reply.component';

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    ForumComponent,
    ForumThreadsComponent,
    ForumThreadInnerComponent,
    NewThreadComponent,
    BaseComponent,
    ReplyComponent
  ],
  imports: [
    BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
    AppRoutingModule
  ],
  providers: [SharedHttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
