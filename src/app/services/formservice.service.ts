import {Injectable} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class FormService {

    constructor(private fb: FormBuilder) {
    }


    Get(myObject, params = {}, arraySizes = {}): FormGroup {
        console.log(arraySizes);

        let props = {};
        // let myObject = new type;
        for (const key in myObject) {

            if (key.toString() !== 'id') {
                if (typeof myObject[key] === 'object' && !(myObject[key] instanceof Date) && !Array.isArray(myObject[key])) {
                    props[key] = this.Get(myObject[key]);
                } else if (Array.isArray(myObject[key])) {
                    console.log('we have array');

                    const formArray = [];
                    let arraySize = 1;
                    if (arraySizes[key]) {

                        arraySize = arraySizes[key];
                    }
                    for (let i = 0; i < arraySize; i++) {
                        for (const innerkey in myObject[key]) {
                            formArray.push(this.Get(myObject[key][innerkey]));
                        }
                    }

                    props[key] = this.fb.array(formArray);


                } else {
                    props[key] = [myObject[key], Validators.required];
                }
            }
        }
        if (params) {
            Object.keys(params).forEach(key => {


                props[key.toString()] = params[key];

            });
        }


        const res = this.fb.group(props);
        console.log(res);
        return res;
    }
}
