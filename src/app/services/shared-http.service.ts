import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {throwError} from 'rxjs';
import {environment} from '../../environments/environment';
import {Tables, Include} from '../../models/Tables';
import {UserAuthentication} from '../../models/UserAuthentication';
import {AuthenticationResult} from '../../models/AuthenticationResult';


@Injectable()
export class SharedHttpService {


    private apiBaseUrl;

    constructor(private httpClient: HttpClient) {
        this.apiBaseUrl = environment.apiBaseUrl;
    }

    async Get<T>(query: Array<{ table: Tables, key: number }> | { table: Tables, key: number },
                 include: Array<Include> | Include = Include.none,
                 where: any = null) {
        try {

            let queryString = '';
            if (!(query instanceof Array)) {
                query = [query];
            }
            query.forEach(queryParam => {
                queryString += '/' + Tables[queryParam.table] + '/' + (queryParam.key > 0 ? queryParam.key : '');
            });
            if (!(include instanceof Array)) {
                include = [include];
            }
            if (include[0] !== Include.none || where !== null) {
                queryString += '?filter={"include":[';
                include.forEach(inc => {
                    queryString += '\"' + Include[inc] + '\"' + ',';


                });
                // apart from this, each querystring line must contain +=
                queryString = queryString.substring(0, queryString.length - 1);
                queryString += ']';
                if (where !== null) {
                    if (include[0] !== Include.none) {
                        queryString += ',"where":';

                    } else {
                        queryString += '"where":';
                    }
                    queryString += JSON.stringify(where);


                }
                queryString += '}'; // where ends here
                queryString += '&access_token=' + this.getToken();
            } else {
                queryString += '?access_token=' + this.getToken();
            }
            queryString = this.apiBaseUrl + queryString;
            console.log(queryString);

            const data = await this.httpClient.get<T>(queryString).toPromise();
            return {Success: true, data: data};
        } catch (e) {
            return {Success: false, data: e};
        }


    }

    // GET /appointments?filter={"include":["patient"],"where":{"physicianId":1}}
    async GetRelated<T>(baseTable: Tables, baseTableId: number, table: Tables, tableId: number = -1,
                        include: Array<Include> | Include = Include.none, where = null) {
        try {

            let queryString = '';
            queryString += '/' + Tables[baseTable] + '/' + baseTableId + '/' + Tables[table];

            if (tableId !== -1) {
                queryString += '/' + tableId;
            }
            if (!(include instanceof Array)) {
                include = [include];
            }
            if (include[0] !== Include.none || where !== null) {
                queryString += '?filter={"include":[';
                include.forEach(inc => {
                    queryString += '\"' + Include[inc] + '\"' + ',';


                });
                // apart from this, each querystring line must contain +=
                queryString = queryString.substring(0, queryString.length - 1);
                queryString += ']';
                console.log(queryString);
                if (where !== null) {
                    if (include[0] !== Include.none) {
                        queryString += ',"where":';

                    } else {
                        queryString += '"where":';
                    }
                    queryString += JSON.stringify(where);
                    queryString += '}'; // where ends here

                } else {
                    queryString += '}';

                }
                queryString += '&access_token=' + this.getToken();
            } else {
                queryString += '?access_token=' + this.getToken();
            }
            queryString = this.apiBaseUrl + queryString;

            console.log(queryString);
            const data = await this.httpClient.get(queryString).toPromise();
            return {Success: true, data: data};
        } catch (e) {
            return {Success: false, data: e};
        }


    }


    async GetRelatedWithWhere<T>(baseTable: Tables, baseTableId: number, table: Tables,
                                 include: Array<Include> | Include = Include.none, where: null) {
        try {
            const tableId = -1;
            let queryString = '';
            queryString += '/' + Tables[baseTable] + '/' + baseTableId + '/' + Tables[table];

            if (tableId !== -1) {
                queryString += '/' + tableId;
            }
            if (!(include instanceof Array)) {
                include = [include];
            }
            if (include[0] !== Include.none || where !== null) {
                queryString = '?filter={"include":[';
                include.forEach(inc => {
                    queryString += '\"' + inc + '\"' + ',';
                });
                queryString = queryString.substring(queryString.length - 1);
                queryString += ']';
                if (where !== null) {
                    queryString += ',"where:"';
                    queryString += JSON.stringify(where);
                    queryString += '}';

                }
                queryString += '}&access_token=' + this.getToken();
            } else {
                queryString += '?access_token=' + this.getToken();
            }
            queryString = this.apiBaseUrl + queryString;

            console.log(queryString);
            const data = await this.httpClient.get(queryString).toPromise();
            return {Success: true, data: data};
        } catch (e) {
            return {Success: false, data: e};
        }


    }


    async Authenticate(credentials: UserAuthentication) {
        try {
            const data = await this.httpClient.post<AuthenticationResult>(this.apiBaseUrl + '/people/login', credentials).toPromise();
            console.log(data);
            return {Success: true, data: data};
        } catch (e) {
            return {Success: false, data: e};
        }

    }

    async PostRelation<T>(table: Tables, data: any, relateWithTable: Tables, relateWithTableId: number) {
        try {
            const url = this.apiBaseUrl + '/' +
                Tables[relateWithTable] + '/' + relateWithTableId + '/' + Tables[table]
                + '?access_token=' + this.getToken();
            console.log('url', url);
            const result = await this.httpClient.post<T>
            (url, data).toPromise();
            return {Success: true, data: result};
        } catch (e) {
            return {Success: false, data: e};
        }


    }


    getToken() {
        return JSON.parse(localStorage.getItem('user'))['token'];

    }


    async Patch<T>(table: Tables, data: any, rowId: number) {
        try {
            const result = await this.httpClient.patch<T>
            (this.apiBaseUrl + '/' + Tables[table] + '/'
                + rowId + '?access_token=' + this.getToken(), data).toPromise();
            return {Success: true, data: result};
        } catch (e) {
            return {Success: false, data: e};
        }


    }

    async PatchRelation<T>(table: Tables, data: any, relateWithTable: Tables, relateWithTableId: number, rowId: number) {
        try {

            const result = await this.httpClient.patch<T>
            (this.apiBaseUrl + '/' +
                Tables[relateWithTable] + '/' + relateWithTableId + '/' + Tables[table] + '/'
                + rowId + '?access_token=' + this.getToken(), data).toPromise();
            return {Success: true, data: result};
        } catch (e) {
            return {Success: false, data: e};
        }


    }

    async Post<T>(table: Tables, data: any) {
        try {

            const result = await this.httpClient.post<T>
            (this.apiBaseUrl + '/' + Tables[table] + '/' + '?access_token=' + this.getToken(), data).toPromise();
            return {Success: true, data: result};
        } catch (e) {
            return {Success: false, data: e};
        }

    }


}


