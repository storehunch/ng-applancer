import {Component, OnInit} from '@angular/core';
import {SharedHttpService} from '../services/shared-http.service';
import {Tables} from '../../models/Tables';
import {ForumThread} from '../../models/ForumThread';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-forum-threads',
    templateUrl: './forum-threads.component.html',
    styles: []
})
export class ForumThreadsComponent implements OnInit {

    forumThreads: ForumThread;
    id: number;

    constructor(private httpService: SharedHttpService, private route: ActivatedRoute) {
    }

    async ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        await this.getItems(null);
    }

    async getItems(where: any | null) {
        const res = await this.httpService.GetRelated(Tables.ForumTopics, this.id, Tables.ForumThreads, -1);

        if (res.Success) {
            console.log('forums threads', res);
            this.forumThreads = res.data;
        } else {
            console.log('err', res);
        }
    }


}
