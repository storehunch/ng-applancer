import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumThreadsComponent } from './forum-threads.component';

describe('ForumThreadsComponent', () => {
  let component: ForumThreadsComponent;
  let fixture: ComponentFixture<ForumThreadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumThreadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumThreadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
