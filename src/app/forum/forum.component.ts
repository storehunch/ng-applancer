import {Component, OnInit} from '@angular/core';
import {ForumTopic} from '../../models/ForumTopic';
import {SharedHttpService} from '../services/shared-http.service';
import {Tables} from '../../models/Tables';

@Component({
    selector: 'app-forum',
    templateUrl: './forum.component.html',
    styles: []
})
export class ForumComponent implements OnInit {

    forums: ForumTopic;

    constructor(private httpService: SharedHttpService) {
    }

    async ngOnInit() {
        await this.getItems(null);
    }

    async getItems(where: any | null) {
        const res = await this.httpService.Get({table: Tables.ForumTopics, key: -1});

        if (res.Success) {
            console.log('forums data', res);
            this.forums = res.data;
        } else {
            console.log('err', res);
        }
    }


}
