import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumThreadInnerComponent } from './forum-thread-inner.component';

describe('ForumThreadInnerComponent', () => {
  let component: ForumThreadInnerComponent;
  let fixture: ComponentFixture<ForumThreadInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumThreadInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumThreadInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
