import {Component, OnInit} from '@angular/core';
import {SharedHttpService} from '../services/shared-http.service';
import {Include, Tables} from '../../models/Tables';
import {ForumThread} from '../../models/ForumThread';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-forum-thread-inner',
    templateUrl: './forum-thread-inner.component.html',
    styles: []
})
export class ForumThreadInnerComponent implements OnInit {

    forumThread: ForumThread;
    id: number;
    tid: number;

    constructor(private httpService: SharedHttpService, private route: ActivatedRoute) {
    }

    async ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        this.tid = this.route.snapshot.params['threadId'];
        await this.getItems(null);
    }

    async getItems(where: any | null) {
        const res = await this.httpService.Get({table: Tables.ForumThreads, key: this.tid}, Include.replies);

        if (res.Success) {
            console.log('forum thread single', res);
            this.forumThread = res.data;
        } else {
            console.log('err', res);
        }
    }


}
