import {Component, OnInit} from '@angular/core';
import {ForumTopic} from '../../models/ForumTopic';
import {SharedHttpService} from '../services/shared-http.service';
import {Tables} from '../../models/Tables';
import {FormGroup} from '@angular/forms';
import {ForumThread} from '../../models/ForumThread';
import {FormService} from '../services/formservice.service';
import {Person} from '../../models/Person';
import {BaseComponent} from '../BaseComponent/BaseComponent';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-new-thread',
    templateUrl: './new-thread.component.html',
    styles: []
})
export class NewThreadComponent extends BaseComponent implements OnInit {
    forums: ForumTopic;
    form: FormGroup;
    user: Person;
    id: number;
    tid: number;

    constructor(private formService: FormService, private router: Router, private httpService: SharedHttpService,
                private route: ActivatedRoute) {
        super(router);
    }

    async ngOnInit() {

        this.id = this.route.snapshot.params['id'];
        this.tid = this.route.snapshot.params['threadId'];

        if (this.id) {
            await this.getItems(null);
        }

        this.form = this.formService.Get(new ForumThread(this.user.id));
    }

    async getItems(where: any | null) {
        const res = await this.httpService.Get({table: Tables.ForumTopics, key: -1});
        if (res.Success) {
            console.log('forums topics', res);
            this.forums = res.data;
        } else {
            console.log('err', res);
        }
    }

    async submit() {
        console.log('val', this.form.value);
        const forumId = this.form.value.forumTopicId;
        const res = await this.httpService.PostRelation<ForumThread>
        (Tables.ForumThreads, this.form.value, Tables.ForumTopics, forumId);
        console.log('thread posted', res);
        if (res.Success) {
            this.router.navigate(['/forum/' + forumId + '/thread/' + res.data.id]);
        }
    }

}
