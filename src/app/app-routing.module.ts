import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SigninComponent} from './signin/signin.component';
import {SignupComponent} from './signup/signup.component';
import {ForumComponent} from './forum/forum.component';
import {ForumThreadsComponent} from './forum-threads/forum-threads.component';
import {ForumThreadInnerComponent} from './forum-thread-inner/forum-thread-inner.component';
import {NewThreadComponent} from './new-thread/new-thread.component';
import {ReplyComponent} from './reply/reply.component';

const routes: Routes = [

    {path: 'signup', component: SignupComponent},
    {path: 'signin', component: SigninComponent},
    {path: 'forum', component: ForumComponent},
    {path: 'forum/:id/threads', component: ForumThreadsComponent},
    {path: 'forum/:id/thread/:threadId', component: ForumThreadInnerComponent},
    {path: 'forum/:id/thread/:threadId', component: ForumThreadInnerComponent},
    {path: 'new-thread', component: NewThreadComponent},
    {path: 'new-thread', component: NewThreadComponent},
    {path: 'forum/:id/thread/:threadId/reply', component: ReplyComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
