import {Component, OnInit} from '@angular/core';
import {SharedHttpService} from '../services/shared-http.service';
import {Tables} from '../../models/Tables';
import {FormGroup} from '@angular/forms';
import {ForumThread} from '../../models/ForumThread';
import {FormService} from '../services/formservice.service';
import {Person} from '../../models/Person';
import {BaseComponent} from '../BaseComponent/BaseComponent';
import {ActivatedRoute, Router} from '@angular/router';
import {Reply} from '../../models/Reply';

@Component({
    selector: 'app-reply',
    templateUrl: './reply.component.html',
    styles: []
})
export class ReplyComponent extends BaseComponent implements OnInit {
    forumThread: ForumThread;
    form: FormGroup;
    user: Person;
    id: number;
    threadId: number;

    constructor(private formService: FormService, private router: Router, private httpService: SharedHttpService,
                private route: ActivatedRoute) {
        super(router);
    }

    async ngOnInit() {

        this.id = this.route.snapshot.params['id'];
        this.threadId = this.route.snapshot.params['threadId'];

        await this.getItems(null);

        this.form = this.formService.Get(new Reply(this.user.id, this.threadId, 'Re: ' + this.forumThread.title));
    }

    async getItems(where: any | null) {
        const res = await this.httpService.GetRelated(Tables.ForumTopics, this.id, Tables.ForumThreads, this.threadId);

        if (res.Success) {
            console.log('forum thread single', res);
            this.forumThread = res.data;
        } else {
            console.log('err', res);
        }
    }

    async submit() {
        console.log('val', this.form.value);
        const res = await this.httpService.PostRelation<Reply>
        (Tables.Replies, this.form.value, Tables.ForumThreads, this.threadId);
        console.log('thread reply', res);
        if (res.Success) {
            this.router.navigate(['/forum/' + this.id + '/thread/' + this.threadId]);
        }
    }

}
