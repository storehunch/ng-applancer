import {Component, OnInit} from '@angular/core';
import {Person} from '../../Models/Person';
import {Router} from '@angular/router';

@Component({
    selector: 'app-base',
    template: '<div></div>',
    styles: []
})

export class BaseComponent implements OnInit {

    user: Person;

    constructor(router: Router) {
        this.user = JSON.parse(localStorage.getItem('user'));

        if (!this.user) {
            // router.navigate(['/signin']);
        }
    }


    ngOnInit() {

    }


}


