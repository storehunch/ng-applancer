import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

import {SharedHttpService} from '../services/shared-http.service';
import {Include, Tables} from '../../Models/Tables';
import {Router} from '@angular/router';
import {Person} from '../../Models/Person';
import {UserAuthentication} from '../../Models/UserAuthentication';
import {FormService} from '../services/formservice.service';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styles: []
})
export class SigninComponent implements OnInit {

    form: FormGroup;

    constructor(private formService: FormService, private httpService: SharedHttpService, private router: Router) {
    }

    async ngOnInit() {

        this.form = this.formService.Get(new UserAuthentication());

    }


    async submit() {

        const res = await this.httpService.Authenticate(this.form.value);
        if (!res.Success) {
            return;
        }
        const getUser = await this.httpService.Get<Person>({
            table: Tables.People,
            key: res.data.userId
        }, [Include.company]);
        if (!getUser.Success) {
            return;
        }
        getUser.data.token = res.data.id;
        localStorage.setItem('user', JSON.stringify(getUser.data));

        this.router.navigate(['/forum']);


    }



}
