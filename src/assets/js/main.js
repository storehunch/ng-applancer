jQuery(function($) {
    "use strict";

    var $body = $('body'),
        $window = $(window),
        $header = $('.doc-header');

    function navBgSetter() {
        if($(".nav-bg-merger").length){
            $body.addClass('no-nav-bg');
            $header.css('margin-bottom', -1 * $header.innerHeight() + 'px');
            $(".nav-bg-merger").css('padding-top',  $header.innerHeight() + 'px');
        }
    }

    $window.on('resize', function () {
        setTimeout(function () {
            navBgSetter();
        },150);
    }).resize();


    $('.slider-wrapper').slick({
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    });

});