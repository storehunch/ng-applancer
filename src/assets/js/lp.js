jQuery(function($) {
    "use strict";


    var isSubscribe = localStorage.getItem('pre-subscribed');
    if(isSubscribe){
        $(".cta-form-wrap").html('<div class="text-left cta-message"><strong>' +  isSubscribe + '</strong>,<p>You will be notified about our launch, Thank you for subscribing.</p></div>');
    }

    $("a[href^='#']").on('click',function (e) {
        var $this = $(this),
            target = $this.attr('href');
        if($(target).length){
            e.preventDefault();
            $('body,html').animate({scrollTop: $(target).offset().top - 100 + 'px'});
        }
    });

    window.onSubmit = function() {
        // alert('aaaa');
    }

    $('#pre-register').attr('novalidate', true).off('submit').on('submit', function(e){
        // grecaptcha.execute();
        submitForm(e);
    });

    function submitForm (e) {
        // return false so form submits through jQuery rather than reloading page.


        if (e.preventDefault) e.preventDefault();
        else e.returnValue = false;

        var body = $('body'),
            thisForm = $('#pre-register'),
            formAction = typeof thisForm.attr('action') !== typeof undefined ? thisForm.attr('action') : "",
            submitButton = thisForm.find('button[type="submit"], input[type="submit"]'),
            error = 0,
            originalError = thisForm.attr('original-error'),
            captchaUsed = thisForm.find('div.recaptcha').length ? true : false,
            successRedirect, formError, formSuccess, errorText, successText;

        body.find('.form-error, .form-success').remove();
        submitButton.attr('data-text', submitButton.text());
        errorText = thisForm.attr('data-error') ? thisForm.attr('data-error') : "Please fill all fields correctly";
        successText = thisForm.attr('data-success') ? thisForm.attr('data-success') : "Thanks, we'll be in touch shortly";
        body.append('<div class="form-error" style="display: none;">' + errorText + '</div>');
        body.append('<div class="form-success" style="display: none;">' + successText + '</div>');
        formError = body.find('.form-error');
        formSuccess = body.find('.form-success');
        thisForm.addClass('attempted-submit');


        if (typeof originalError !== typeof undefined && originalError !== false) {
            formError.text(originalError);
        }

        error = validateFields(thisForm);

        if (error === 1) {
            alert('error')
        }
        else {

            thisForm.removeClass('attempted-submit');

            // Hide the error if one was shown
            formError.fadeOut(200);

            // Create a new loading spinner in the submit button.
            submitButton.addClass('btn--loading');

            console.log(thisForm.serialize());

            jQuery.ajax({
                type: "POST",
                url: "https://script.google.com/macros/s/AKfycbx2QG1xhfwRDmVhrj81WbXcqkSA-CPxISjziDV50K6-Rk3qttw/exec",
                data: thisForm.serialize() + "&url=" + window.location.href + "&captcha=" + captchaUsed,
                success: function (response) {
                    // Swiftmailer always sends back a number representing number of emails sent.
                    // If this is numeric (not Swift Mailer error text) AND greater than 0 then show success message.

                    submitButton.removeClass('btn--loading').hide();
                    $(".form-submit-message").html("Thank You!");

                    localStorage.setItem('pre-subscribed', $(".company-field").val());

                },
                error: function (errorObject, errorText, errorHTTP) {
                    // Keep the current error text in a data attribute on the form
                    formError.attr('original-error', formError.text());
                    // Show the error with the returned error text.
                    formError.text(errorHTTP).stop(true).fadeIn(1000);
                    formSuccess.stop(true).fadeOut(1000);
                    submitButton.removeClass('btn--loading');
                }
            });
        }

        return false;
    };



    var validateFields = function(form) {
        var body = $(body),
            error = false,
            originalErrorMessage,
            name,
            thisElement;

        form = $(form);




        form.find('.validate-required[type="checkbox"]').each(function() {
            var checkbox = $(this);
            if (!$('[name="' + $(this).attr('name') + '"]:checked').length) {
                error = 1;
                name = $(this).attr('data-name') ||  'check';
                checkbox.parent().addClass('field-error');
                //body.find('.form-error').text('Please tick at least one ' + name + ' box.');
            }
        });

        form.find('.validate-required, .required, [required]').not('input[type="checkbox"]').each(function() {
            if ($(this).val() === '') {
                $(this).addClass('field-error');
                error = 1;
            } else {
                $(this).removeClass('field-error');
            }
        });

        form.find('.validate-email, .email, [name*="cm-"][type="email"]').each(function() {
            if (!(/(.+)@(.+){2,}\.(.+){2,}/.test($(this).val()))) {
                $(this).addClass('field-error');
                error = 1;
            } else {
                $(this).removeClass('field-error');
            }
        });

        form.find('.validate-number-dash').each(function() {
            if (!(/^[0-9][0-9-]+[0-9]$/.test($(this).val()))) {
                $(this).addClass('field-error');
                error = 1;
            } else {
                $(this).removeClass('field-error');
            }
        });

        // Validate recaptcha
        if(form.find('div.recaptcha').length && typeof form.attr('data-recaptcha-sitekey') !== typeof undefined){
            thisElement = $(form.find('div.recaptcha'));
            if(grecaptcha.getResponse(form.data('recaptchaWidgetID')) !== ""){
                thisElement.removeClass('field-error');
            }else{
                thisElement.addClass('field-error');
                error = 1;
            }
        }

        if (!form.find('.field-error').length) {
            body.find('.form-error').fadeOut(1000);
        }else{

            var firstError = $(form).find('.field-error:first');

            if(firstError.length){
                $('html, body').stop(true).animate({
                    scrollTop: (firstError.offset().top - 100)
                }, 1200, function(){firstError.focus();});
            }
        }



        return error;
    };

});